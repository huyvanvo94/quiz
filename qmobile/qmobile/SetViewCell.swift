//
//  SetViewCell.swift
//  qmobile
//
//  Created by Huy Vo on 4/1/19.
//  Copyright © 2019 Huy Vo. All rights reserved.
//

import UIKit



@IBDesignable
class SetViewCell: UITableViewCell {
    static let reuseIdentifier = "SetViewCell"

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var aboutLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
