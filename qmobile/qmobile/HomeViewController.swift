//
//  ViewController.swift
//  qmobile
//
//  Created by Huy Vo on 3/6/19.
//  Copyright © 2019 Huy Vo. All rights reserved.
//

import UIKit

let sets = [
    Set(title: "Example1", about: "Example"),
    Set(title: "Example2", about: "Example")
]

class HomeViewController: UIViewController {
    
    // Stores sets of app
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
    }
}

// MARK: UITableViewDelegate
extension HomeViewController: UITableViewDelegate {
    
}

// MARK: UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let set = sets[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: SetViewCell.reuseIdentifier) as! SetViewCell
        
        cell.titleLabel.text = set.title
        cell.aboutLabel.text = set.about
        
        return cell
    }
    
    
}

