//
//  Set.swift
//  qmobile
//
//  Created by Huy Vo on 4/1/19.
//  Copyright © 2019 Huy Vo. All rights reserved.
//

import Foundation

/*
 
 */
class Set: NSObject {
    
    var title: String
    var about: String
 
    init(title: String, about: String) {
        self.title = title
        self.about = about
    }
}
